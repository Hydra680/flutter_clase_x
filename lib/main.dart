//https://medium.com/@manuelvargastapia/entrenando-con-flutter-parte-2-4fb8d166ee94
//page the project model
import 'package:flutter/material.dart';
import 'package:flutter_clase_x/page/HomePage.dart';
import 'package:flutter_clase_x/page/LoginPage.dart';
import 'package:flutter_clase_x/page/SignupPage.dart';

void main() => runApp(MyApp());

// * this is a Highliht comment
// ! this is a alert comment
// ? this is a query comment
// TODO: this is a todo comment

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/SingUp',
      routes: {
        '/':(context) => LoginPage(),
        '/SingUp':(context) => SignupPage(),
        '/Home':(context) => HomePage(),
      },
    );
  }
}
