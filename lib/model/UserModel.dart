class UserModel {
  int _id;
  String _name;
  String _email;
  String _password;
  int _raiting;

  UserModel(
      {int id=1,String name, String email,  String pass, int raitin=0}) {
    this._name = name;
    this._email = email;
    this._password = pass;
    this._raiting = raitin;
  }

  String get name => this._name;
  String get email => this._email;
  String get pass => this._password;
  int get raitin => this._raiting;
  int get id => this._id;

  void setRainting(int value){
    this._raiting =value;
  }

  @override
  String toString() {
    
    return "$_id ,$_name,$_email,$_password,$_raiting";
  }


/*
  id INTEGER PRYMARY KEY AUTOINCREMENT,
            name TEXT,
            email TEXT,
            password TEXT,
            raiting INTEGER
          
*/
   Map<String, dynamic> toMap(){
    return {
      'id': this._id,
      'name': this._name,
      'email': this._email,
      'password': this._password,
      'raiting': this._raiting,
    };
  }

  UserModel.fromMap(Map<String, dynamic> data){
    this._id = data['id'];
    this._name = data['name'];
    this._email = data['email'];
    this._password = data['password'];
    this._raiting = data['raiting'];
  }


  
}
