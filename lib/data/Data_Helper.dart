//importar libreria sqflite
import 'package:flutter_clase_x/model/UserModel.dart';
import 'package:sqflite/sqflite.dart';

class DataHelper {
  static const TABLE_NAME = 'usuario';

  Future<Database> db = openDatabase('dbUser.db', //nombre de la base de datos
      version: 1, onCreate: (Database db, int version) async {
    await db.execute("""
          CREATE TABLE $TABLE_NAME(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            email TEXT,
            password TEXT,
            raiting INTEGER
          );""");
  });

  //CRUD
  Future<int> insertUser(UserModel userModel) async {
    Database db = await this.db;
    int resp = await db.insert(TABLE_NAME, userModel.toMap());
    print("#################################");
    print(">>>>> INSERT EXECUTE");
    print("#################################");
    return resp;
  }

  Future<List<UserModel>> readUser() async {
    Database db = await this.db;
    List<Map<String, dynamic>> resp = await db.query(TABLE_NAME);
    final lista = resp.map((Map<String, dynamic> data) {
      return UserModel.fromMap(data);
    }).toList();

    print("#################################");
    print(">>>>> READ EXECUTE");
    print("#################################");

    return lista.isEmpty ? [] : lista;
  }

  Future deleteUserId(int id) async {
    Database db = await this.db;

    int resp = await db.delete(TABLE_NAME, where: "id = ?", whereArgs: [id]);

    print("#################################");
    print(">>>>> DELETE EXECUTE $resp");
    print("#################################");
  }

  Future updateUser(UserModel userModel) async {
    Database db = await this.db;

    int resp = await db.update(TABLE_NAME, userModel.toMap(),
        where: "id = ?", whereArgs: [userModel.id]);

    print("#################################");
    print(">>>>> UPDATE EXECUTE $resp");
    print("#################################");
  }
}
