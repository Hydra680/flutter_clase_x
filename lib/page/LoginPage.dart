import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LoginPage extends StatelessWidget {
  
  void _portraitModeOnly() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }
  
  @override
  Widget build(BuildContext context) {
    print(MediaQuery.of(context).orientation);
    _portraitModeOnly();
    return Scaffold(
      appBar: _appBar(),
      body: _logIn(),
    );
  }

  Widget _appBar() {
    return AppBar(
      title: Text("Log In"),
    );
  }

  Widget _logIn() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 35),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          TextField(
            decoration: InputDecoration(labelText: "Usuario"),
            keyboardType: TextInputType.emailAddress,
          ),
          SizedBox(
            height: 25,
          ),
          TextField(
            decoration: InputDecoration(labelText: "Contraseña"),
            keyboardType: TextInputType.visiblePassword,
            obscureText: true,
          ),
          SizedBox(
            height: 25,
          ),
          RaisedButton(
            child: Text("Log In"),
            onPressed: () {},
          ),
          RaisedButton(
            child: Text("Sign Up"),
            onPressed: () {},
          ),
        ],
      ),
    );
  }
}
