import 'package:flutter/material.dart';
import 'package:flutter_clase_x/data/Data_Helper.dart';
import 'package:flutter_clase_x/model/UserModel.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'package:qr_flutter/qr_flutter.dart';
import 'package:sqflite/sqflite.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // UserModel objRecibido;

  DataHelper db = DataHelper();

  @override
  Widget build(BuildContext context) {
    //recibimos el objeto
    //objRecibido = ModalRoute.of(context).settings.arguments;
    //print("###################  Acepted  ############################");
    //print("                  $objRecibido");
    //print("#########################################################");

    return Scaffold(
      appBar: AppBar(),
      body: _listBuilder(),
    );
  }

  Widget _body() {
    return Container(
      child: Column(
        children: <Widget>[
          _user(),
        ],
      ),
    );
  }

  Widget _listBuilder() {
    return FutureBuilder(
      future: db.readUser(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<UserModel> data = snapshot.data;
          return data.isEmpty
              ? //preguntamos si la lista esta vacia
              Center(
                  child: Text("Data Empty"),
                ) //mostramo text si es vacio
              : ListView.builder(
                  itemCount: data.length, // generamos una lista
                  itemBuilder: (context, int i) {
                    //genera _user a partir de data en posision i
                    // ? return Container();
                    return _user(userModel: data[i]);
                  },
                );
        }

        //obtenemos la lista a partir de snapshot que hace referencia a un readuser que es un future
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  Widget _user({UserModel userModel}) {
    print("$userModel ---------------------");
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      height: 250,
      child: Card(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Expanded(
              flex: 1,
              //manejamos el qrImage
              child: QrImage(
                backgroundColor: Colors.white,
                data: "${userModel.name}",
                version: QrVersions.auto,
                size: 130.0,
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Usuario: ${userModel.name}",
                      style: TextStyle(fontSize: 25),
                    ),
                    Text(
                      "email: ${userModel.email}",
                      style: TextStyle(fontSize: 20),
                    ),
                    Text(
                      "pass: ${userModel.pass}",
                      style: TextStyle(fontSize: 20),
                    ),
                    //manejamos el raitingbar
                    RatingBar(
                      initialRating: userModel.raitin.toDouble(),
                      minRating: 1,
                      itemSize: 30,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {
                        //TODO: actualizamos rting
                        userModel.setRainting(rating.toInt());
                        db.updateUser(userModel);
                        print(rating);
                      },
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
